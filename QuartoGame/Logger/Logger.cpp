#include "Logger.h"

Logger::Logger(std::ostream& outstream, Level level)
	:outstream(outstream), minimumLevel(level)
{
}

void Logger::Log(const std::string& msg, Level level)
{
	if (level>=minimumLevel)
	  outstream << msg<< "\n";
}
