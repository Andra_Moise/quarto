#pragma once
#include<iostream>
#include<string>
#include "UnusedPieces.h"
#include "Board.h"

class Player
{
private:
	std::string name;
public:
	Player(std::string);
	friend std::ostream& operator<<(std::ostream&, const Player&);
	Piece PickPiece( std::istream& in, UnusedPieces& unusedP) const;
	void PlacePiece( std::istream& in, Board& board, Piece&& piece) const;
};

