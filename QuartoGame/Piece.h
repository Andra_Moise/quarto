#pragma once
#include<iostream>
#include<cstdint>

class Piece
{
public:
	enum class Color : uint8_t
	{
		white,
		black
	};

	enum class Shape : uint8_t
	{
		round,
		square
	};

	enum class Height : uint8_t
	{
		Short,
		tall
	};

	enum class BodyStyle : uint8_t
	{
		full,
		hollow
	};

	Piece(Color c, Height h, Shape s, BodyStyle bds)
		:m_color(c), m_height(h), m_shape(s), m_bodystyle(bds)
	{

	}

	Piece()=default;

	Color getColor() const;
	Shape getShape() const;
	Height getHeight() const;
	BodyStyle getBodystyle() const;

	friend std::ostream& operator<<(std::ostream& print, const Piece& piece);

private:

	Color m_color :1;
	Shape m_shape :1;
	Height m_height :1;
	BodyStyle m_bodystyle :1; 
};

