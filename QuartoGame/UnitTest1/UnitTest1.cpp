#include "pch.h"
#include "CppUnitTest.h"
#include "../Piece.h"
#include "../Piece.cpp"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(PieceConstructor)
		{
			Piece piece1(Piece::Color::black, Piece::Height::Short, Piece::Shape::square, Piece::BodyStyle::hollow);
			Assert::IsTrue(Piece::BodyStyle::hollow == piece1.getBodystyle());
			//Assert::AreEqual(Piece::Color::black, piece1.getColor());
		}
	};
}
