#include "quatroGame.h"

void quatroGame::Run()
{
	Board board;
	std::string name;
	std::cout << "Introduceti numele pt player1: \n";
	std::cin >> name;
	Player player1(name);
	std::cout << "Introduceti numele pt player2: \n";
	std::cin >> name;
	Player player2(name);
	UnusedPieces unused_Pieces;
	std::reference_wrapper<Player> pickingPlayer = player1;
	std::reference_wrapper<Player> choosingPlayer=player2;

	while (!board.isFull())
	{
	   system("cls");
	   std::cout << board << std::endl;
	   std::cout<<"Piese disponibile: "<<std::endl;
	   std::cout << unused_Pieces;
	   std::cout << "\n";
	   Piece piece;
	   while (true)
	   {
		   std::cout << choosingPlayer << "trebuie sa aleaga o piesa ";
		   try {

			   piece = std::move(choosingPlayer.get().PickPiece(std::cin, unused_Pieces));
			   break;

		   }
		   catch (const char* msg)
		   {
			   std::cout << msg;
		   }
	   }

	   while (true)
	   {
		   std::cout << pickingPlayer << " trebuie sa aleaga o pozitie ";
		   try {
			   pickingPlayer.get().PlacePiece(std::cin, board, std::move(piece));
			   break;
		   }
		   catch (const char* msg)
		   {
			   std::cout << msg;
		   }
	   }
	   std::swap(pickingPlayer, choosingPlayer);
	}
}
