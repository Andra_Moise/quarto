#include "Piece.h"

Piece::Color Piece::getColor() const
{
    return this->m_color;
}

Piece::Shape Piece::getShape() const
{
    return this->m_shape;
}

Piece::Height Piece::getHeight() const
{
    return this->m_height;
}

Piece::BodyStyle Piece::getBodystyle() const
{
    return this->m_bodystyle;
}

std::ostream& operator<<(std::ostream& print, const Piece& piece)
{
    return print << static_cast<uint16_t>(piece.m_color)<< static_cast<uint16_t>(piece.m_shape)
        << static_cast<uint16_t>(piece.m_height)<<static_cast<uint16_t>(piece.m_bodystyle);
}
