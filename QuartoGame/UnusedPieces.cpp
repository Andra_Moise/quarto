#include "UnusedPieces.h"

void UnusedPieces::Initialize()
{
	Emplace(Piece(Piece::Color::white, Piece::Height::Short, Piece::Shape::round, Piece::BodyStyle::full));
	Emplace(Piece(Piece::Color::white, Piece::Height::Short, Piece::Shape::round, Piece::BodyStyle::hollow));
	Emplace(Piece(Piece::Color::white, Piece::Height::tall, Piece::Shape::round, Piece::BodyStyle::full));
	Emplace(Piece(Piece::Color::white, Piece::Height::tall, Piece::Shape::round, Piece::BodyStyle::hollow));
	Emplace(Piece(Piece::Color::white, Piece::Height::Short, Piece::Shape::square, Piece::BodyStyle::full));
	Emplace(Piece(Piece::Color::white, Piece::Height::Short, Piece::Shape::square, Piece::BodyStyle::hollow));
	Emplace(Piece(Piece::Color::white, Piece::Height::tall, Piece::Shape::square, Piece::BodyStyle::full));
	Emplace(Piece(Piece::Color::white, Piece::Height::tall, Piece::Shape::square, Piece::BodyStyle::hollow));

	Emplace(Piece(Piece::Color::black, Piece::Height::Short, Piece::Shape::round, Piece::BodyStyle::full));
	Emplace(Piece(Piece::Color::black, Piece::Height::Short, Piece::Shape::round, Piece::BodyStyle::hollow));
	Emplace(Piece(Piece::Color::black, Piece::Height::tall, Piece::Shape::round, Piece::BodyStyle::full));
	Emplace(Piece(Piece::Color::black, Piece::Height::tall, Piece::Shape::round, Piece::BodyStyle::hollow));
	Emplace(Piece(Piece::Color::black, Piece::Height::Short, Piece::Shape::square, Piece::BodyStyle::full));
	Emplace(Piece(Piece::Color::black, Piece::Height::Short, Piece::Shape::square, Piece::BodyStyle::hollow));
	Emplace(Piece(Piece::Color::black, Piece::Height::tall, Piece::Shape::square, Piece::BodyStyle::full));
	Emplace(Piece(Piece::Color::black, Piece::Height::tall, Piece::Shape::square, Piece::BodyStyle::hollow));


}

void UnusedPieces::Emplace(const Piece& piece)
{
	std::stringstream ss;
	ss << piece;
	m_unusedPieces.insert(std::make_pair(ss.str(), piece));
}

UnusedPieces::UnusedPieces()
{
	Initialize();
}

Piece UnusedPieces::choosePiece(const std::string& key)
{
	auto extracted = m_unusedPieces.extract(key);
	if (extracted)
		return std::move(extracted.mapped());
	else 
		throw "Piece not found \n";
}

std::ostream& operator<<(std::ostream& output, const UnusedPieces& up)
{
	for (auto unusedPice : up.m_unusedPieces)
	{
		output << unusedPice.first << " ";
		output << "\n";
	}
	return output;
}
