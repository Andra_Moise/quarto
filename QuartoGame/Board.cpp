#include "Board.h"
#include<tuple>


std::optional<Piece>& Board::operator[](const Position& positionReceive)
{
    /*uint8_t line= positionReceive.first, column=positionReceive.second;
    std::tie(line, column) = positionReceive;*/
    const auto&[line, column] = positionReceive;
    if (line >= 4 || column >= 4 || line < 0 || column < 0)
        throw "Pozitie invalida \n";
    return m_board[line*4+column];
}

const std::optional<Piece>& Board::operator[](const Position& positionReceive) const
{
    /*uint8_t line = positionReceive.first, column = positionReceive.second;
    std::tie(line, column) = positionReceive;*/
    const auto&[line, column] = positionReceive;
    if (line >= 4 || column >= 4 || line < 0 || column < 0)
        throw "Pozitie invalida \n";
    return m_board[line * 4 + column];
}

bool Board::isFull() const
{
    return std::all_of(m_board.begin(), m_board.end(), [](const std::optional<Piece>optionaPiece)
        {return optionaPiece.has_value(); });
}


std::ostream& operator<<(std::ostream& print, const Board& m_board)
{
    Board::Position p;
    auto& [line, column] = p;
   /* uint8_t line = p.first;
    uint8_t column = p.second;*/
    for (line = 0; line < 4; line++)
    {
        for (column = 0; column < 4; column++)
        {
            if (m_board[p])
                print << *m_board[p];
            else
                print << "----";
            print << " ";
        }
        print << std::endl;
    }
    return print;
}
