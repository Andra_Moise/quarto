#include "pch.h"
#include "../Piece.h"
#include "../Piece.cpp"

TEST(TestCaseName, TestName) {
  
	Piece piece1(Piece::Color::black, Piece::Height::Short, Piece::Shape::square, Piece::BodyStyle::hollow);
  EXPECT_EQ(Piece::Color::black, piece1.getColor());
  EXPECT_TRUE(Piece::Color::black == piece1.getColor());
}