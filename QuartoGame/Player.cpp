#include "Player.h"

Player::Player(std::string name)
	:name(name)
{
}

Piece Player::PickPiece(std::istream& in, UnusedPieces& unusedP) const
{
	std::string piece_name;
	in >> piece_name;
	Piece piece = unusedP.choosePiece(piece_name);
	return piece;
}

void Player::PlacePiece(std::istream& in, Board& board, Piece&& piece) const
{
	int line, col;
	if (in >> line)
	{
		if (in >> col)
		{
			board[{line, col}] = std::move(piece);
			return;
		}
	}
	throw "Introduce doar 2 numere\n";
}

std::ostream& operator<<(std::ostream& output, const Player& player1)
{
	output << player1.name<<" ";
	return output;
}
